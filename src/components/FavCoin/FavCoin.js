import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { Color, Icon } from '../../common';
import _ from 'lodash';
import IonIcons from 'react-native-vector-icons/Ionicons';

class FavCoin extends Component {
  constructor(props) {
    super(props);
  }

  routeToCoinInfo(item) {
    this.props.navigation.push({
      screen: 'CoinInfo',
      title: item.name,
      passProps: {
        id: item.id
      }
      
    })
  }

  renderItem(item, index) {
   
    return (
      <View key={item.rank} style={{ marginTop: 10,marginHorizontal: 15 }}>
        <View style={{flex: 1,flexDirection: 'row'}}>
          <TouchableOpacity onPress={()=> this.routeToCoinInfo(item)} style={{flex: 3,flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Text style={{color: '#fff',fontSize: 16}}>{index + 1}</Text>
            </View>
            <View style={{flex: 6}}>
              <Text style={{ color: '#fff', fontSize: 16 }}>{item.name}</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => this.props.removeFavCoin(item.id)}>
              <IonIcons name={Icon.close} size={30} color={'#fff'} />
            </TouchableOpacity>
          </View>
        </View>
        
      </View>


    );
  }
  render() {
    const { favCoins, } = this.props;
    const { favData } = favCoins;
    const coinData = favData.sort(function (a, b) {
      return parseFloat(a.rank) - parseFloat(b.rank);
    });
    return (
      <View style={{ flex: 1, backgroundColor: Color.appContainerColor }}>
        <View>
          <FlatList
            data={coinData}
            renderItem={({ item, index }) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

export default FavCoin;