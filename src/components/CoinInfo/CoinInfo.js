import React, { Component } from 'react';
import {
  View,
  Text,
  WebView,
  TouchableOpacity,
  Switch,
  ActivityIndicator,
  Dimensions,
  Platform
} from 'react-native';
import { Color, Images } from '../../common';
import NavBar from '../NavBar/NavBar';
import { VictoryArea, VictoryChart, VictoryLine, VictoryPie } from 'victory-native';
import numeral from 'numeral';

const width = Dimensions.get('window').width

class CoinInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentGraphSelect: '1m',
      coinData: null,
      isFetching: false
    }
  }
  componentDidMount() {
    this.fetchCoinChartData(this.props.allCoinData.symbol, 'histoday', '60');
  }

  async fetchCoinChartData(coin, graphType, limit) {
    this.setState({ isFetching: true})
    let link = await fetch(`https://min-api.cryptocompare.com/data/${graphType}?tsym=USD&limit=${limit}&fsym=${coin}`)
    let data = await link.json();
    let allCoinChat = data.Data;
    const coinData = allCoinChat.map((data) => { return data.high })
    this.setState({coinData,isFetching: false})
  }

  renderGraph(symbol) {
    if (symbol === 'MIOTA') {
      return (
        <View />
      )
    }
    else {
      if (this.state.isFetching) {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator
              size='large'
            />
          </View>

        )

      }
      else {
        return (
          <View style={{ flex: 1 }}>
            <VictoryArea
              interpolation='cardinal'
              padding={15}
              height={250}
              width={width}
              style={{
                data: { fill: "#4AE08C" },
              }}
              data={this.state.coinData}
            />
          </View>

        )
      }
    }
      
    
  }

  onIconClicked(navigation) {
    navigation.pop({})
  }

  render() {
    const { navigation, onValueChange, value, allCoinData } = this.props;
    const { name, price_usd, market_cap_usd, total_supply, max_supply, symbol, } = allCoinData;
    const nameSymbol = name.length > 14 ? symbol : name;
    const priceUSD = numeral(price_usd).format('0,0.00'),
      marketCap = numeral(market_cap_usd).format('0,0'),
      curculatingSupply = numeral(total_supply).format('0,0'),
      totalSupply = numeral(max_supply).format('0,0');
      totalVolume = numeral(allCoinData['24h_volume_usd']).format('0,0');
    
    const month_selected = this.state.currentGraphSelect === '1m' ? '#4AE08C' : '#fff',
      day_selected = this.state.currentGraphSelect === '1d' ? '#4AE08C' : '#fff',
      hour_selected = this.state.currentGraphSelect === '1h' ? '#4AE08C' : '#fff';
    
    return (
      <View style={{ backgroundColor: Color.appContainerColor, flex: 1 }}>
        <View>
          <NavBar
            title={name}
            navIcon={Images.close}
            onIconClicked={() => this.onIconClicked(navigation)}
          />
        </View>
        <View style={{ flex: 0.2, marginTop: 5,marginHorizontal: 15, }}>
          <View style={{ flex: 1,flexDirection: 'row' }}>
            <View style={{ flex: 1, justifyContent: 'center'}}>
              <Text style={{ color: '#FFECDB',fontSize: 18 }}>Add To Favourites</Text>
            </View>
            <View style={{flex: 1,justifyContent: 'center'}}>
              <Switch
                onValueChange={onValueChange}
                value={value}
                onTintColor="#F4A460"
              />
            </View>
          </View>
        </View>
        <View style={{ flex: 0.5, marginHorizontal: 15 }}>
          <View style={{flex: 1}}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={styles.coinInfoName}>
                <Text style={{ color: Color.propertyNameColor, fontSize: 24 }}>{nameSymbol}</Text>
              </View>
              <View style={styles.coinInfoData}>
                <Text style={{ color: Color.propertyDataColor, fontSize: 25 }}>$ {priceUSD}</Text>
              </View>
            </View>
          </View>  
          <View style={{marginTop: 10,flex: 3}}>
            <View style={{ flexDirection: 'row', flex: 1 }}>
              <View style={styles.coinInfoName}>
                <Text style={styles.coinInfoNameText}>Market Cap</Text>
              </View>
              <View style={styles.coinInfoData}>
                <Text style={styles.coinInfoDataText}>${marketCap} USD</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', flex: 1,marginTop: 5 }}>
              <View style={styles.coinInfoName}>
                <Text style={styles.coinInfoNameText}>Volume (24h)</Text>
              </View>
              <View style={styles.coinInfoData}>
                <Text style={styles.coinInfoDataText}>{totalVolume}</Text>  
              </View>
            </View>
            <View style={{ flexDirection: 'row', flex: 1, marginTop: 5  }}>
              <View style={styles.coinInfoName}>
                <Text style={styles.coinInfoNameText}>Circulating Supply</Text>
              </View>
              <View style={styles.coinInfoData}>
                <Text style={styles.coinInfoDataText}>{curculatingSupply} {symbol}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', flex: 1, marginTop: 5  }}>
              <View style={styles.coinInfoName}>
                <Text style={styles.coinInfoNameText}>Max Supply</Text>
              </View>
              <View style={styles.coinInfoData}>
                <Text style={styles.coinInfoDataText}>{totalSupply} {symbol}</Text>
              </View>
            </View>
          </View>
          
        </View>
        <View style={{ flex: 1,marginTop: 15 }}>
          {this.renderGraph(symbol)}
        </View>
        <View style={{ flex: 0.3, marginHorizontal: 100,height: 10 }}>
          <View style={{ flex: 1, flexDirection: 'row', }}>
            <TouchableOpacity
              onPress={() => {
                this.fetchCoinChartData(symbol, 'histoday', '60');
                this.setState({ currentGraphSelect: '1m'})
              }}
              style={{ flex: 1, alignItems: 'center' }}>
              <Text style={{ color: month_selected }}>1m</Text>
            </TouchableOpacity>  
            
            <TouchableOpacity
              onPress={() => {
                this.fetchCoinChartData(symbol, 'histohour', '48');
                this.setState({ currentGraphSelect: '1d'})
              }}
              style={{ flex: 1, alignItems: 'center' }}>
              <Text style={{color: day_selected }}>1d</Text>
            </TouchableOpacity>  
            
            <TouchableOpacity
              onPress={() => {
                this.fetchCoinChartData(symbol, 'histominute', '120');
                this.setState({ currentGraphSelect:'1h'})
              }}
              style={{ flex: 1, alignItems: 'center', }}
            >
              <Text style={{ color: hour_selected }}>1h</Text>
            </TouchableOpacity>  

          </View>
        </View>

      </View> 
    );
  }
}

const styles = {
  coinInfoName: {
    flex: 1
  },
  coinInfoNameText: {
    color: '#F27927',
    fontSize: 14
  },
  coinInfoData: {
    flex: 1
  },
  coinInfoDataText: {
    color: '#E6CC22',
    fontSize: 14
  }
}

export default CoinInfo;