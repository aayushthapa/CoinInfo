import React, { Component } from 'react';
import {
  TouchableOpacity
} from 'react-native'

class ButtonPlatform extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { onPress, style, children,key } = this.props;
    return (
      <TouchableOpacity
        key={key}
        onPress={onPress}
        style={style}
      >
        {children}
      </TouchableOpacity>
    );
  }
}

export default ButtonPlatform;