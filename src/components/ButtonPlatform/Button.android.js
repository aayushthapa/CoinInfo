import React, { Component } from 'react';
import {
  TouchableNativeFeedback,
  View
} from 'react-native';

class ButtonPlatform extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { onPress, children, style,key } = this.props;
    return (
      <TouchableNativeFeedback
        key={key}
        onPress={onPress}
        background={TouchableNativeFeedback.SelectableBackground()}
        style={style}
      >
        <View style={{padding: 15}}>
         {children} 
       </View>
      </TouchableNativeFeedback>
    );
  }

}

export default ButtonPlatform;