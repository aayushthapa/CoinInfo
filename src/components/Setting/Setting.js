import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  TouchableNativeFeedback,
  ActionSheetIOS,
  AsyncStorage,
  StatusBar
} from 'react-native';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';
import ButtonPlatform from '../ButtonPlatform/Button';
import App from '../../App';
import { Color, Images } from '../../common';
import NavBar from '../NavBar/NavBar';

const BUTTONS = [
  '100',
  '200',
  '350',
  '500',
  '700',
  '1000',
  'Cancel'
]

const DIALOG_BUTTONS_ANDROID = [
  '100',
  '200',
  '350',
  '500',
  '700',
  '1000',
]

const CANCEL_INDEX = 6

class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: 100,
      dialogBoxVisible: false,
      singlePickerVisible: false,
      singlePickerSelectedItem: null
    }
  }

  async componentWillUnmount() {
    try {
      await AsyncStorage.setItem('size', this.state.clicked.toString());
    }
    catch (e) {

    }
  }

  renderDialogBox() {
    return (
      <SinglePickerMaterialDialog
        title={'Pick one element!'}
        items={DIALOG_BUTTONS_ANDROID.map((row, index) => ({ value: index, label: row }))}
        visible={this.state.singlePickerVisible}
        selectedItem={this.state.singlePickerSelectedItem}
        onCancel={() => this.setState({ singlePickerVisible: false })}
        onOk={(result) => {
          this.setState({ singlePickerVisible: false });
          this.setState({ singlePickerSelectedItem: result.selectedItem });
        }}
      />
    );
    /*
    return (
      <Dialog
        title="Stack Size"
        style={{ height: 400 }}
        actions={[
          <DialogButton text='CANCEL' onPress={()=> this.refs.dialog.close()} />
        ]}
        ref='dialog'
      >
        <View style={{ paddingHorizontal: 16 }}>
          {
            DIALOG_BUTTONS_ANDROID.map((item, index) => {
              return (
                <ButtonPlatform
                  key={index}  
                  style={{ padding: 15 }}
                  onPress={() => {
                    this.setState({ clicked: item });
                    this.refs.dialog.close()
                    }
                  }
                >
                  <View >
                    <Text>{item}</Text>
                  </View>
                </ButtonPlatform>
              );
            })
          }
        </View>
      </Dialog>
    ); */
  }

  showActionSheet() {
    ActionSheetIOS.showActionSheetWithOptions({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX
    },
      (buttonText) => {
        this.setState({ clicked: BUTTONS[buttonText] })
      }
    );


  }
  
  onIconClicked(navigation) {
    navigation.push({
      screen: 'MarketContainer'
    });
  }

  render() {
    const { navigation } = this.props;
    console.log(this.state.singlePickerSelectedItem)
    return (
      <View style={{ backgroundColor: Color.appContainerColor, flex: 1 }}>
        <View>
          <NavBar
            title='Setting'
            navIcon={Images.back}
            onIconClicked={() => this.onIconClicked(navigation)}
          />
        </View>
        <View>
          <ButtonPlatform
            style={{ padding: 15 }}
            onPress={() => { Platform.OS === 'ios' ? this.showActionSheet() : this.setState({ singlePickerVisible: true }) }}
          >
            <View>
              <Text style={{ color: '#fff', fontSize: 16 }}>Stack Size</Text>
            </View>
          </ButtonPlatform>
          <ButtonPlatform
            style={{ padding: 15 }}
            onPress={() => alert('sdfd')}
          >
            <View>
              <Text style={{ color: '#fff', fontSize: 16 }}>Contribute</Text>
            </View>
          </ButtonPlatform>
        </View>
        {this.renderDialogBox()}
        <View>
          <Text>{this.state.clicked}</Text>
        </View>
      </View>
    );
  }
}

export default Setting;