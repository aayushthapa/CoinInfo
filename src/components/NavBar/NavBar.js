import React, { Component } from 'react';
import {
  View,
  ToolbarAndroid,
  Platform,
  StatusBar
} from 'react-native';
import PropTypes from 'prop-types';
import { Color } from '../../common';

export default class NavBar extends Component {
  constructor(props) {
    super(props);
  }
  static defaultProps = {
    backgroundColor: Color.navBarColor,
    titleColor: '#fff'

  }
  render() {
    const {
      backgroundColor,
      actions,
      title,
      titleColor,
      onActionSelected,
      navIcon,
      onIconClicked
    } = this.props;
    if (Platform.OS === 'android') {
      return (
        <View>
          <StatusBar
            backgroundColor={Color.statusBarColor}
            barStyle='light-content'
          />
          <ToolbarAndroid
            style={{ backgroundColor: backgroundColor, height: 56, elevation: 4 }}
            title={title}
            titleColor={titleColor}
            actions={actions}
            onActionSelected={onActionSelected}
            navIcon={navIcon}
            onIconClicked={onIconClicked}
          />
        </View>

      );
    }

    else {
      return (
        <StatusBar
          backgroundColor={Color.statusBarColor}
          barStyle='light-content'
        />
      )
    }

  }
}

