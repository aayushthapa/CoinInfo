import React, { Component } from 'react';
import { 
  View,
  Text
} from 'react-native';
import { Color } from '../../common';
import FavCoin from '../../components/FavCoin/FavCoin';
import { connect } from 'react-redux';
import * as removeFavCoin from '../../redux/actions';
 
class FavCoinContainer extends Component {
  static navigatorStyle = {
    navBarBackgroundColor: Color.navBarColor,
    navBarTextColor: '#ddd',
    navBarButtonColor: '#ddd',
    statusBarColor: Color.statusBarColor
  }

  constructor(props) {
    super(props);
  }
  render() {
    return (
      <FavCoin favCoins={this.props.favcoin} removeFavCoin={this.props.removeFavCoin} navigation={this.props.navigator} />
    )
  }
}

mapStateToProps = ( state ) => {
  return {
    favcoin: state.favCoin
  }
}

export default connect(mapStateToProps,removeFavCoin ) (FavCoinContainer);