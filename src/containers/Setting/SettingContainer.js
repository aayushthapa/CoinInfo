import React, { Component } from 'react';
import { 
  View,
  Text,
  Platform
} from 'react-native';
import { Color } from '../../common';
import Setting from '../../components/Setting/Setting';
 
class SettingContainer extends Component {
  static navigatorStyle = {
    ...Platform.select({
      ios: {
        navBarHidden: false,
        navBarButtonColor: '#ddd',
      },
      android: {
        navBarHidden: true
      }
    })
    
  }
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Setting navigation={this.props.navigator} />
    );
  }
}

export default SettingContainer;