import React, { Component } from 'react';
import { Platform } from 'react-native';
import CoinInfo from '../../components/CoinInfo/CoinInfo';
import { connect } from 'react-redux';
import { addCoin,removeFavCoin } from '../../redux/actions'
import { coinChartData }from '../../redux/actions';
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import { Color } from '../../common';

class CoinInfoContainer extends Component {
  constructor(props) {
    super(props);
  }
  static navigatorStyle = {
     ...Platform.select({
      ios: {
        navBarHidden: false,
        navBarButtonColor: '#ddd',
      },
      android: {
        navBarHidden: true,
        statusBarColor: Color.statusBarColor
      }
    }),
    navBarBackgroundColor: Color.navBarColor,
    navBarTextColor: '#ddd',
    navBarButtonColor: '#ddd'
  }

  onValueChange(favCoin, allCoinData,id) {
    this.checkCoinIsLiked(favCoin, id) ? this.props.removeFavCoin(id) : this.props.addCoin(allCoinData);
    
  }
  checkCoinIsLiked(favCoin,id) {
    const index = _.findIndex(favCoin, function (coin) {
      return coin.id == id;
    });
    return index >= 0;
  
  }
  render() {
    const { favCoin, id, item, name, chartDatas, coinChartData } = this.props;
    const allCoinData = _.find(this.props.allCoinInfo.market, (allCoin) => { return allCoin.id === this.props.id })
    return (
      <CoinInfo
        id={id}
        coinChart={chartDatas}
        onValueChange={() => this.onValueChange(favCoin.favData, allCoinData,id)}
        value={this.checkCoinIsLiked(favCoin.favData, id)}
        allCoinData={allCoinData}
        coinChartData={coinChartData}
        navigation={this.props.navigator}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    favCoin: state.favCoin,
    chartDatas: state.coinChart,
    allCoinInfo: state.market
  }
}

const  mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addCoin: addCoin,
    removeFavCoin: removeFavCoin,
    coinChartData: coinChartData,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps) (CoinInfoContainer);