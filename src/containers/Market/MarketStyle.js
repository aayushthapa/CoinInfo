export default styles = {
  headerStyle: {
    flex: 1,
    flexDirection: 'row',
  },
  rank: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  nameHeaderComponent: {
    justifyContent: 'center',
    flex: 1.5,
    alignItems: 'flex-start'
  },
  headerFont: {
    fontSize: 13,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff'
  },
  coinInfoStyle: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10
  },
  coinRank: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  priceInfoComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  coinInfoComponent: {
    flex: 1.5,
    justifyContent: 'center'
  },
  coinInfoFont: {
    fontSize: 12,
    color: '#fff'
  }
}