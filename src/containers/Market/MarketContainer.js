import React, { PureComponent,Component } from 'react';
import {
  View,
  Text,
  FlatList,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  AsyncStorage,
  StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import numeral from 'numeral'
import {fetchMarket,fetchMarketCap} from '../../redux/actions/';
import IonIcons from 'react-native-vector-icons/Ionicons';
import { Icon,Color,Images } from '../../common';
import styles from './MarketStyle';
import NavBar from '../../components/NavBar/NavBar';

const totalCoinDisplay = [
  { limit: 100, start: 0, display: '100' },
  { limit: 50, start: 100, display: '100-150' },
  { limit: 100, start: 150, display: '150-250' },
  { limit: 100, start: 250, display: '250-350' },
  { limit: 150, start: 350, display: '350-500' },
  { limit: 500, start: 500, display: '>500' },
]

class MarketContainer extends PureComponent {

  static navigatorStyle = {
    ...Platform.select({
      ios: {
        navBarHidden: false,
        navBarButtonColor: '#ddd',
      },
      android: {
        navBarHidden: true
      }
    }),
    navBarBackgroundColor: Color.navBarColor,
    navBarTextColor: '#ddd',
    navBarButtonColor: '#ddd'
  }

  static navigatorButtons = {
    ...Platform.select({
      ios: {
        rightButtons: [
          {
            title: 'Favourites',
            id: 'fav',
            icon: Images.fav,
            buttonColor: '#fff'
          }
        ]
      },
    })
  }


  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: false,
      selectRange: '100'
    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }


  async componentDidMount() {
    /*
    let value = await AsyncStorage.getItem('size');
    this.setState({ limit: value }) */
    this.props.fetchMarket(0,100);
    this.props.fetchMarketCap();

  }

  onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'fav') {
        this.props.navigator.push({
          screen: 'FavCoinContainer',
          title: 'Favourites'
        }) 
      }
    }
  }

  

  routeCoinInfo(item) {
    this.props.navigator.push({
      screen: 'CoinInfo',
      title: item.name,
      passProps: {
        name: item.name,
        id: item.id,
        item: item,
        symbol: item.symbol
      }
    })
  }

  renderItem(item) {
    const textColor = _.includes(item.percent_change_24h, "-") ? '#FF6347' : '#4AE08C'
    const value = (item.price_usd >= 1)? numeral(item.price_usd).format('$0,0.00') : numeral(item.price_usd).format('$0,0.0000')
    return (
      <View key={item.id}>
        <TouchableOpacity
          onPress={() => this.routeCoinInfo(item)}
        >
          <View style={styles.coinInfoStyle}>
            <View style={[styles.coinRank, {}]}>
              <Text style={styles.coinInfoFont}>{item.rank}</Text>
            </View>
            <View style={[styles.coinInfoComponent, {}]}>
              <Text style={styles.coinInfoFont}>{item.name} </Text>
              <Text style={{ color: '#fff' }}>({item.symbol})</Text>
            </View>
            <View style={[styles.priceInfoComponent, {}]}>
              <Text style={styles.coinInfoFont}>{value}</Text>
            </View>
            <View style={[styles.priceInfoComponent, {}]}>
              <Text style={[styles.coinInfoFont, { color: textColor }]}>{item.percent_change_24h} %</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
          
      
      
    )
  }

  async onRefresh() {
    let value = await AsyncStorage.getItem('size');
    this.setState({ refreshing: true })
    this.props.fetchMarket(value);
    this.setState({ refreshing: false})
  }

  renderCoin(isFetching, market) {
    if (isFetching) {
      return (
        <View style={{ flex: 6,alignItems: 'center',justifyContent: 'center' }}>
          <ActivityIndicator size='large' />
        </View>
        
      )
    }
    return (
      <View style={{ flex: 6 }}>
        <FlatList
          data={market}
          renderItem={({ item }) => this.renderItem(item)}
          keyExtractor={(item,index)=> index}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.refreshing}
          ItemSeparatorComponent={() => <View style={{height: 1,backgroundColor: '#ddd'}} />}
        />
      </View>
    )
  }

  onActionSelected(position, navigator) {
    switch (position) {
      case 0:
        navigator.push({
          screen: 'FavCoinContainer',
          title: 'Favourites'
        }) 
      break
    }
  }
  render() {
    const { market, isFetching } = this.props.markets;
    const { marketCap } = this.props.marketCap;
    const {
      navigator
    } = this.props;
    const marketCapValue = numeral(marketCap).format('$0,0.00') 
    return (
      <View style={{ flex: 1, backgroundColor: Color.appContainerColor }}>
        <View>
          <NavBar
            title="Market"
            actions={
              [
                { title: 'Favourites', icon: Images.fav, show: 'always' },
              ]
            }
            onActionSelected={(position) => this.onActionSelected(position, navigator)}
          />
        </View>
        <View style={{ flex: 0.3 }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{ flex: 1.5, justifyContent: 'center', marginLeft:15}}>
              <Text style={{ color: '#F27927'}}>Total Market Cap</Text>
            </View>
            <View style={{flex: 2}}>
              <Text style={{ color: '#E6CC22'}}>{marketCapValue}</Text>
            </View>
          </View>  
            
        </View> 
        <View style={{ flex: 0.2 }}>
          <View style={{ flexDirection: 'row', }}>
            <View style={{flex: 1,justifyContent: 'center',alignItems: 'center'}}>
              <Text style={{ color: '#f1c40f',fontSize: 15}}>Top</Text>
            </View>  
            <View style={{flex: 6,justifyContent: 'center',alignItems: 'center'}}>
              <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
              >
                {
                  totalCoinDisplay.map((item, index) => {
                    const textColor = this.state.selectRange === item.display ? '#f1c40f' :'#fff'
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.fetchMarket(item.start, item.limit);
                          this.setState({
                            selectRange: item.display
                          })
                        }}
                        key={index}
                        style={{ marginLeft: 15 }}
                      >
                        <Text style={{ color: textColor, fontSize: 12 }}>{item.display}</Text>
                      </TouchableOpacity>

                    )
                  })
                }
              </ScrollView>
            </View>
          </View>  
          
        </View>
        <View style={{ flex: 0.5,backgroundColor: Color.headerColor,marginTop: 10  }}>
          <View style={styles.headerStyle}>
            <View style={[styles.rank, {}]}>
              <Text style={styles.headerFont}>#</Text>
            </View>
            <View style={[styles.nameHeaderComponent,{}]}>
              <Text style={[styles.headerFont,{marginLeft: 15}]}>Name</Text>
            </View>
            <View style={[styles.headerComponent, {}]}>
              <Text style={styles.headerFont}>Price</Text>
            </View>
            <View style={[styles.headerComponent, {}]}>
              <Text style={styles.headerFont}>% Change(24 h)</Text>
            </View>
          </View>
        </View>
        <View style={{ flex: 6 }}>
          {this.renderCoin(isFetching,market)}
          
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    markets: state.market,
    marketCap: state.marketCap
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchMarket: fetchMarket,
    fetchMarketCap: fetchMarketCap
   }, dispatch)
 }

export default connect(mapStateToProps, mapDispatchToProps)(MarketContainer);