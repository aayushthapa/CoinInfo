import { Navigation } from 'react-native-navigation';
import { registerScreens } from './screens';
import { Color } from './common';

registerScreens();


Navigation.startSingleScreenApp({
  screen: {
    screen: 'MarketContainer',
    title: 'Market',
    navigatorStyle: {
      navBarHidden: true,
      statusBarColor: Color.statusBarColor
    }
  }
})

