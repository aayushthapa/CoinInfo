import { Platform } from 'react-native'; 

export default Images = {
  ...Platform.select({
    ios: {
      fav: require('../assets/images/ios/favios.png'),
      setting: require('../assets/images/ios/settingios.png')
    },
    android: {
      fav: require('../assets/images/android/fav.png'),
      setting: require('../assets/images/android/setting.png'),
    }
  }),
  
  close: require('../assets/images/android/close.png'),
  
}