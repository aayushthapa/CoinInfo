import Color from './Color';
import Images from './Images';
import Icon from './Icon';

export {
  Color,
  Images,
  Icon
}
