import { Platform } from 'react-native';

export default Icon = {
  ...Platform.select({
    android: {
      refresh: 'md-refresh',
      fav: 'md-heart',
      close: 'md-close',
      next: 'ios-arrow-forward',
      back: 'ios-arrow-back'
    },
    ios: {
      refresh: 'ios-refresh',
      fav: 'ios-heart',
      close: 'ios-close',
      next: 'ios-arrow-forward',
      back: 'ios-arrow-back'
    } 
  })
}