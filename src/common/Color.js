export default Colors = {
  statusBarColor: '#162027',
  navBarColor: '#1E2B34',
  appContainerColor: '#2C3A47',
  headerColor: '#34495e',
  whiteColor: '#fff',
  blackColor: '#000',
  propertyNameColor: '#F27927',
  propertyDataColor: '#E6CC22'
}