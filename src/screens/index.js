import React ,{ Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import hoistNonReactStatic from 'hoist-non-react-statics';
import configureStore from '../redux/store';

import {
  MarketContainer,
  GraphContainer,
  SettingContainer,
  FavCoinContainer,
  CoinInfo
} from '../containers';


rnnavigationWithHOC = (Components, store) =>{
  class HOC extends Component{
    render() {
      return (
        <Provider store={store}>
          <Components {...this.props} />
        </Provider>
      )
    }
  }
  hoistNonReactStatic(HOC,Components)
  return HOC;
}
const store = configureStore()

export const registerScreens = () => {
  Navigation.registerComponent('MarketContainer', () => rnnavigationWithHOC(MarketContainer, store) );
  Navigation.registerComponent('SettingContainer', () => rnnavigationWithHOC(SettingContainer, store));
  Navigation.registerComponent('GraphContainer', () => rnnavigationWithHOC(GraphContainer, store));
  Navigation.registerComponent('FavCoinContainer', () => rnnavigationWithHOC(FavCoinContainer, store));
  Navigation.registerComponent('CoinInfo', () => rnnavigationWithHOC(CoinInfo, store));
}