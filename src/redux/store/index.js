import { createStore,applyMiddleware,compose } from 'redux'
import thunk from 'redux-thunk';
import { AsyncStorage } from 'react-native';
import { persistStore, autoRehydrate } from 'redux-persist'
import reducers from '../reducers';

export default configureStore = () => {
  let store = createStore(
    reducers,
    {},
    compose(
      applyMiddleware(thunk),
      autoRehydrate()
    )
    
  );
  persistStore(store, { storage: AsyncStorage, blacklist: ['market,coinChart'] });
  return store;
}