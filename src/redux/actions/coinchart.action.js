import { COIN_CHAT_DATA_FETCHING,COIN_CHART_DATA_SUCCESS } from '../../config/constant';


const buildURL = (coin,graphType,limit) => {
  return `https://min-api.cryptocompare.com/data/${graphType}?tsym=USD&limit=${limit}&fsym=${coin}`
}
export const coinChartData = (coin, graphType, limit) => {
  
  return async (dispatch) => {
    try {
      let url = buildURL(coin,graphType,limit)
      let link = await fetch(url)
      let data = await link.json();
      let coin_Data = data.Data;
      const allCoinChat = coin_Data.map((data)=> { return data.high})
      dispatch({
        type: COIN_CHART_DATA_SUCCESS,
        payload: allCoinChat
      });
    }
    catch(e){
      console.error(e)
    }
  }
}