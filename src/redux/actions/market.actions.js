import { AsyncStorage } from 'react-native';

import { FETCHED_MARKET_SUCCESS, FETCH_MARKET_ERROR, FETCHING_MARKET,FETCH_MARKET_CAP } from '../../config/constant';

const buildURl = (start,limit) => {
  return `https://api.coinmarketcap.com/v1/ticker/?start=${start}&limit=${limit}`
}

export const fetchMarket = (start,value) => {
  return async (dispatch) => {
    dispatch({
      type: FETCHING_MARKET
    });
    try {
      const url = buildURl(start,value);
      let data = await fetch(url);
      let marketData = await data.json();
      dispatch({ type: FETCHED_MARKET_SUCCESS, payload: marketData })
    }
    catch (e) {
      dispatch({ type: FETCH_MARKET_ERROR, payload: e })
    }
  }
}

export const fetchMarketCap = () => {
  return async (dispatch) => {
    try {
      let url = await fetch('https://api.coinmarketcap.com/v1/global/');
      let data = await url.json();
      let marketCap = data.total_market_cap_usd
      dispatch({
        type: FETCH_MARKET_CAP,
        payload: marketCap
      })
    }
    catch(e){
      console.error(e)
    }
  }
}




