import { ADD_FAV_COIN, REMOVE_FAV_COIN } from '../../config/constant';
import { AsyncStorage } from 'react-native';
export const addCoin = (coin) => {
  return (dispatch) => {
    dispatch({
      type: ADD_FAV_COIN,
      payload: coin,
    });
  };
};

export const removeFavCoin = (index) => {
  return async (dispatch) => {
    dispatch({
      type: REMOVE_FAV_COIN,
      payload: index,
    })
  }
}