export { fetchMarket,fetchMarketCap } from './market.actions';
export { addCoin,removeFavCoin } from './favcoin.action';
export { coinChartData } from './coinchart.action';