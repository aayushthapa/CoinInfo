import { ADD_FAV_COIN, REMOVE_FAV_COIN } from '../../config/constant';
import { AsyncStorage } from 'react-native';
import { REHYDRATE } from 'redux-persist/constants'
import _ from 'lodash';

const initialState = {
  favData: [],
  value: false
}

export default (state = initialState, action) => {
  const { type, payload, data,value } = action;
  switch (type) {
    case ADD_FAV_COIN:
      return {
        ...state,
        favData: _.uniqBy([payload, ...state.favData || []], 'id')
      } 
    
    case REMOVE_FAV_COIN:
      return {
        ...state,
        favData: state.favData.filter((fav, i) => fav.id !== payload)
      }
      
  }
  return state;  
}
