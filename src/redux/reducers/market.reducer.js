import { FETCHED_MARKET_SUCCESS,FETCH_MARKET_ERROR,FETCHING_MARKET } from '../../config/constant';

const initialState = {
  isFetching: false,
}

export default (state = {initialState}, action) => {
  switch (action.type) {
    case FETCHING_MARKET:
      return {
        ...state,
        isFetching: true,
      }  
    case FETCHED_MARKET_SUCCESS:
      return {
        ...state,
        isFetching: false,
        market: action.payload
      }  
    case FETCH_MARKET_ERROR: 
      return {
        ...state,
        isFetching: false,
        market: action.payload
      }  
    default: 
      return state  
  }
}