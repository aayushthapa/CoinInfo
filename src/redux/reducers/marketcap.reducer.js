import { FETCH_MARKET_CAP } from '../../config/constant';

const initialState = {
  marketCap: ''
}


export default (state={initialState},action) => {
  switch (action.type) {
    case FETCH_MARKET_CAP:
      return {
        ...state,
        marketCap: action.payload
      }  
    default:
      return state  
  }
}