import { COIN_CHAT_DATA_FETCHING,COIN_CHART_DATA_SUCCESS } from '../../config/constant';

const initialState = {
  coinData: [],
  isFetching: false
}

export default (state = initialState, action) => {
  
  switch (action.type) { 
    case COIN_CHART_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        coinData: action.payload
      }
    default:
      return state;  
  }
}