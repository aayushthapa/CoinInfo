import { combineReducers } from 'redux';
import market from './market.reducer';
import marketCap from './marketcap.reducer';
import favCoin from './favcoin.reducer';
import coinChart from './coinchart.reducer';

export default combineReducers({
  market,
  marketCap,
  favCoin,
  coinChart,
})